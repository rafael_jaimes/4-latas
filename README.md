# 4 Latas 🚀

### Pre-requisitos 📋

_Todo se ejecuta en raiz del proyecto_

### Instalación 🔧

_Descargar librerias necesarias para docker (solo la primera vez)_

```
  docker-compose build
```

_Ejecutar-para levantar server:_

```
  docker-compose up
```

_Instalar node_

```
  npm install
```

_Instalar dependencias_

```
  composer install
```    

### Instalar base de datos 🔧

_Acceder a app de docker para poder ejecutar las migraciones_

```
  docker-compose exec app bash
```

_Instalar las migraciones (si hay modificaciones en bd, actualizar las migraciones)_

```
  php artisan migrate
```

_Insertar datos:_

```
  php artisan db:seed
```

### Compilar scripts y sass

_Escucha cambios_

```
  npm run watch
```

_Compila minifica y uglifica en public (paso a produccion de ccs y js )_

```
  npm run production
```

## Construido con 🛠️

* [Laravel](https://laravel.com/) - framework PHP
* [MYSQL](https://www.mysql.com/) - Base de datos
* [Sass](https://sass-lang.com/) - Lenguaje de extensión CSS
* [Jquey](https://rometools.github.io/rome/) - biblioteca de JavaScrip
* [Composer](https://getcomposer.org/) - Manejador de dependencias
* [Node](https://nodejs.org/es/) - Entorno de ejecución para JavaScript

---
⌨️ con ❤️ por **Rafael Jaimes**
