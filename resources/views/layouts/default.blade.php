<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    {{-- Google tag manager --}}
    {{-- @include("snippets.analytics") --}}
    @php
      header("Expires: Tue, 01 Jul 2001 06:00:00 GMT");
      header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
      header("Cache-Control: no-store, no-cache, must-revalidate");
      header("Cache-Control: post-check=0, pre-check=0", false);
      header("Pragma: no-cache");
    @endphp
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>4 Latas</title>
    {{-- metatags --}}
    @include("snippets.metatags")
    {{-- favicons --}}
    @include("snippets.favicons")
    {{-- stylesheets --}}
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    @include("shared._preloader")

    {{-- content --}}
    <div id="main-wrapper">
          @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
