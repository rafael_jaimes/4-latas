@extends('layouts.default')
@section('content')
  <div class="container text-center" id="posRegistry">

    @include('shared._header')

    <div class="col-md-12 contenido">
      <h2>¡Gracias por participar!</h2>
      <h1>EL REGISTRO HA CERRADO</h1>
      <p>Pronto anunciaremos a los ganadores</p>
      <a href="{{ asset('terminos_condiciones/terminos.pdf') }}" target="_blank">Términos y condiciones</a>
      <p><img src="{{ asset('images/4latas-carro2.png') }}" alt=""></p>
    </div>


  </div>


@endsection
