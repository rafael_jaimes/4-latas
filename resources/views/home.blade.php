@extends('layouts.app')

@section('content')
<<<<<<< HEAD
<div class="container-fluid">
=======
<div class="container">
>>>>>>> 25d45d34b38ff911b9ef406bbbf9d50718f2d0fd
    <div class="row justify-content-center">
      <div class="table-responsive">
        <a href="{{ route('reports')}}"  class="btn btn-info">Descargar reporte</a>
        {!! $participants->Render() !!}
        <table class="table table-striped">
          <thead class="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Nombre</th>
              <th scope="col">Apellidos</th>
              <th scope="col">Email</th>
              <th scope="col">DNI</th>
              <th scope="col">Teléfono</th>
              <th scope="col">Imagen Frontal</th>
              <th scope="col">Imagen Trasera</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($participants as $key => $participant)
              <tr>
                <td scope="row">{{ $participant->id }}</td>
                <td>{{ $participant->name }}</td>
                <td>{{ $participant->apellidos }}</td>
                <td>{{ $participant->email }}</td>
                <td>{{ $participant->dni }}</td>
                <td>{{ $participant->phone }}</td>
                <td> <img class="imgTable" src="{{ Storage::url( $participant->imgFront ) }}" alt="{{ $participant->name }}"></td>
                <td> <img class="imgTable" src="{{ Storage::url( $participant->imgBack ) }}" alt="{{ $participant->name }}"></td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
</div>
@endsection
