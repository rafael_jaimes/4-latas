<div class="col-md-12" id="date_info">
  <div class="date">
    {{-- contador descripcion --}}
    <div class="col-md-6">
      <div class="date_description">
        @if ($pre)
          <span>Inicio del registro</span>
          <span>01 DE MARZO</span>
          <span>12:00 AM España</span>
        @else
          <span>Cierre del registro</span>
          <span>04 DE MARZO</span>
          <span>11:59 PM España</span>
        @endif
      </div>
    </div>

    {{-- contador --}}
    <div class="col-md-6">
      <div class="date_counter">
        <span class="faltan">faltan...</span>
        <div id="clock">
        </div>
      </div>
    </div>
  </div>

</div>
