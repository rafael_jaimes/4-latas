
{{-- <script>
  (function(C,i,r,c,u,s){C.GoogleAnalyticsObject=c;C[c]||(C[c]=
  function(){(C[c].q=C[c].q||[]).push(arguments)});C[c].c=+new Date;
  u=i.createElement(r);s=i.getElementsByTagName(r)[0];
  u.src='https://www.google-analytics.com/analytics.js';
  s.parentNode.insertBefore(u,s)}(window,document,'script','ga'));
  ga('create','UA-XXXXX-X','auto');ga('send','pageview');
</script> --}}

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135010833-1"></script>

<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-135010833-1');
</script>
