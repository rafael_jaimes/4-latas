<link rel="apple-touch-icon" sizes="180x180" href="{{ asset("images/favicons/apple-icon.png")}}">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="apple-mobile-web-app-title" content="4 LATAS">
<link rel="manifest" href="{{ asset("images/favicons/manifest.json")}}">
<meta name="mobile-web-app-capable" content="yes">
<meta name="theme-color" content="#060856">
<meta name="application-name" content="4 LATAS">
<meta name="msapplication-TileColor" content="#060856">
<meta name="msapplication-TileImage" content="{{ asset("images/favicons/ms-icon-150x150.png")}}">
<meta name="msapplication-config" content="{{ asset("images/favicons/browserconfig.xml")}}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ asset("images/favicons/favicon-32x32.png")}}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset("images/favicons/favicon-16x16.png")}}">
<link rel="shortcut icon" href="{{ asset("images/favicons/favicon.ico")}}">
