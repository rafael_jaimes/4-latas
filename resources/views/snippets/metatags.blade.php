<?php
	$image = asset("images/4latas-fb.png");
	$title = "Participa para ganar el coche de 4 Latas.";
  $description = "Una Película de Gerardo Olivares.";
?>

<meta name="title" content="{{ $title }}"/>
<meta name="author" content="@Netflix"/>
<meta name="description" content="{{ $description }}"/>
<meta property="og:url" content=""/>
<meta property="og:locale" content="en_US"/>
<meta property="og:type" content="website"/>
<meta property="og:title" content="{{ $title }}"/>
<meta property="og:description" content="{{ $description }}"/>
<meta property="og:image" content="{{ $image }}"/>
<meta name="twitter:card" content="summary_large_image"/>
<meta name="twitter:site" content="@Netflix"/>
<meta name="twitter:creator" content="@Netflix"/>
<meta name="twitter:title" content="{{ $title }}"/>
<meta name="twitter:description" content="{{ $description }}"/>
<meta name="twitter:image" content="{{ $image }}"/>
