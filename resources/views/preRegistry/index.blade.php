@extends('layouts.default')
@section('content')
  <div class="container text-center" id="preRegistry">
    @include('shared._header')
    @include('shared._date')

    <div id="footer" class="col-md-12">
      <p>Necesitarás:</p>
      <p>Foto de tu entrada para ver 4 Latas (a dos caras)</p>
      <a href="{{ asset('terminos_condiciones/terminos.pdf') }}" target="_blank">Términos y condiciones</a>
      <p><img src="{{ asset('images/4latas-carro2.png') }}" alt=""></p>
    </div>


  </div>


@endsection
