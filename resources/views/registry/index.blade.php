@extends('layouts.default')
@section('content')
  <div class="container text-center" id="registry">

    @include('shared._header')

    @include('shared._date')

    @include('registry._form')

    @include('shared._footer')

  </div>


@endsection
