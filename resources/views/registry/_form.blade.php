<div class="col-md-12 text-center" id="form">
  {{-- espacio top y alerta de success --}}
  <div class="top-form">
    @if(session()->has('message'))
      <div id="alert-success">
        {{ session()->get('message') }}
      </div>
    @endif
  </div>


  {{-- formulario --}}
<form method="post" action={{ route('participantes.store')}}  enctype="multipart/form-data" autocomplete="off" >
  @csrf
  <div class="inputs">
    <small class="errors">{{ $errors->first('name', ':message') }}</small>
    <input type="text" name="name" placeholder="NOMBRE" value="{{ old('name') }}" class="{{ $errors->has('name') ? 'is-danger' : '' }}" maxlength="30" required>
  </div>

  <div class="inputs">
    <small class="errors">{{ $errors->first('apellidos', ':message') }}</small>
    <input type="text" name="apellidos" placeholder="APELLIDOS" value="{{ old('apellidos') }}" class="{{ $errors->has('apellidos') ? 'is-danger' : '' }}" maxlength="40" required>
  </div>

  <div class="inputs">
    <small class="errors">{{ $errors->first('dni', ':message') }}</small>
    <input type="text" name="dni" placeholder="NO DE DNI" value="{{ old('dni') }}" class="{{ $errors->has('dni') ? 'is-danger' : '' }}" maxlength="9" required>
  </div>

  <div class="inputs">
    <small class="errors">{{ $errors->first('email', ':message') }}</small>
    <input type="email" name="email" placeholder="EMAIL" value="{{ old('email') }}" class="{{ $errors->has('email') ? 'is-danger' : '' }}" maxlength="50" required>
  </div>

  <div class="inputs">
    <small class="errors">{{ $errors->first('phone', ':message') }}</small>
    <input type="tel" name="phone" placeholder="TELÉFONO" value="{{ old('phone') }}" class="{{ $errors->has('phone') ? 'is-danger' : '' }}"  maxlength="9" required>
  </div>

  <span>Fotos de la entrada</span>

  <div class="files">
    <small class="errors">{{ $errors->first('imgFront', ':message') }}</small>
    <input type="file" id="imgFront" name="imgFront" accept="image/*, application/pdf" data-value="p1" required>
    <label class="{{ $errors->has('imgFront') ? 'is-danger-img' : '' }}" for="imgFront">
      <p id="text_p1">cara frontal</p>
    </label>
  </div>

  <div class="files">
    <small class="errors">{{ $errors->first('imgBack', ':message') }}</small>
    <input type="file" id="imgBack" name="imgBack" accept="image/*, application/pdf" data-value="p2">
    <label class="{{ $errors->has('imgBack') ? 'is-danger-img' : '' }}" for="imgBack">
      <p id="text_p2">cara trasera</p>
    </label>
  </div>

  <button type="submit" class="btn btn-primary">ENVIAR</button>


</form>

</div>
