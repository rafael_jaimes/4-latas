// import {
//     TweenMax
// } from "gsap";

require("gsap");

// var Log = require("./modules/log");

/* instances */
var $ = require("jquery");

var Preloader = require("./modules/preloader");
var Chronometer = require("./modules/chronometer");
var alert = new TimelineMax();
var error = new TimelineMax();

var App = {
    preloader: null,
    init: function() {
        $('html').removeClass('no-js');
        App.preloader = new Preloader("preloader");
        if( $('#preRegistry').length )
          App.chronometer = new Chronometer('mar 1 2019 12:00:00 GMT+1', 'clock', 'Inicio');
        if( $('#registry').length )
          App.chronometer = new Chronometer('mar 04 2019 23:59:00 GMT+1', 'clock', 'Inicio');

        App.addEvents();
    },
    load: function() {
        App.preloader.hide();
        if($('#alert-success').length){
          setTimeout(function() {
            alert.add(TweenMax.to(
                      "#alert-success",0.5,
                      {
                        alpha: 0
                      }
            ));
          },5000);
        }
    },

    addEvents: function() {
        /* Prevent Default in all links with href === '#' */
        $(document).on("click", "a[href='#']", function(event) {
            event.preventDefault();
        });

        $("form").on("submit", function(e) {
            document.body.style.cursor = "wait";
            $("button[type=submit]").attr("disabled", true);
        });

        //

        $("input").focus(function(){
          alert.add(TweenMax.to(
                    $(this).siblings("small"),0.5,
                    {
                      alpha: 0
                    }
          ));
          $(this).removeClass("is-danger");
          // $(this).trigger('blur');
        });

        // quita error de upload
        $("label").click(function(){
          alert.add(TweenMax.to(
                    $(this).siblings("small"),0.5,
                    {
                      alpha: 0
                    }
          ));
          $(this).removeClass("is-danger-img");
          // $(this).trigger('blur');
        });

        // hover
        if (!isMobileDevice()){
          $('button').on('mouseover', function(e) {
              TweenMax.to(e.currentTarget, .5, { backgroundColor: 'rgba(0, 0, 0, .83s)' })
          })
          $('button').on('mouseout', function(e) {
              TweenMax.to(e.currentTarget, .5, { backgroundColor: 'rgba(0, 0, 0, 1)' })
          })

        }



        // file
        $("[type=file]").on("change", function(){
          // Name of file and placeholder
          var file = "listo <b class='successPa'>✔<b>";
          var dflt = $(this).attr("placeholder");
          if($(this).val()!=""){
            $("#text_"+$(this).data( "value" )).html(file);
          } else {
            $(this).next().text(dflt);
          }
        });

        /** Good practice, add toggle class only in 'anchor' or 'button' */
        $(document).on("click", ".toggle", function() {
            var _target = $(this).data("target");
            /**/
            var $target = _target === undefined ? $(this) : $(_target);
            $target.toggleClass('active');
        });


    }
}

function isMobileDevice() {
    return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
}

$(document).ready(App.init());

$(window).on("load", function (e) {
  App.load();
})
