// import {
//     TweenMax
// } from "gsap";
require('gsap');
$ = require('jquery');

class Preloader {
    constructor(args) {
        this.id = args.id || "preloader";
        this.container = document.getElementById(this.id);
        this.init();
    }
    init() {
        document.body.classList.add("loading");
    }
    hide() {
        document.body.classList.remove("loading");
        TweenMax.to(this.container, 0.5, {
            ease: Power2.easeOut,
            delay: 0.8,
            scaleX: 1,
            scaleY: 1,
            opacity: 0,
            onComplete: () => {
              $("#" + this.id).remove();
            }
        });
    }
}
// export default Preloader;
module.exports = Preloader;
