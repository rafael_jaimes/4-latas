// import {
//     TweenMax
// } from "gsap";
require('gsap');
$ = require('jquery');

class Chronometer {

    constructor(deadline,elem,finalMessage) {
        this.countdownInit(deadline,elem,);
        this.countdown(deadline,elem,finalMessage);
    }

    // saca las horas minutos segundos
    getRemainingTime(deadline,elem) {
      let now = new Date(),
      remainTime = (new Date(deadline) - now + 1000) / 1000,
      remainSeconds = ('0' + Math.floor(remainTime % 60)).slice(-2),
      remainMinutes = ('0' + Math.floor(remainTime / 60 % 60)).slice(-2),
      remainHours = ('0' + Math.floor(remainTime / 3600 % 24)).slice(-2),
      remainDays = ('0' + Math.floor(remainTime / (3600 * 24))).slice(-2);
      return {
        remainSeconds,
        remainMinutes,
        remainHours,
        remainDays,
        remainTime
      }
    }

    // contador cambia cada segundo
    countdown(deadline,elem,finalMessage) {
      const el = document.getElementById(elem);

      const timerUpdate = setInterval( () => {

        el.innerHTML = "";

        let t = this.getRemainingTime(deadline,elem);

        clearInterval();

        el.innerHTML = `<div class="contador">
                          <span class="cont_crono">${t.remainDays}</span> <span class="cont_crono">${t.remainHours}</span> <span class="cont_crono">${t.remainMinutes}</span> <span class="cont_crono">${t.remainSeconds}</span> <br>
                        </div>
                        <div class="contador_name">
                          <span class="cont_name">DÍAS</span> <span class="cont_name">HORAS</span> <span class="cont_name">MIN.</span> <span class="cont_name">SEG.</span>
                        </div>
                      `;

        if(t.remainTime <= 1) {
          location.reload();
          clearInterval(timerUpdate);
        }

      }, 1000);
    }

    // inicializa el contador
    countdownInit(deadline,elem){
      let el = document.getElementById(elem);
      let t = this.getRemainingTime(deadline,elem);

      el.innerHTML = `<div class="contador">
                        <span class="cont_crono">${t.remainDays}</span> <span class="cont_crono">${t.remainHours}</span> <span class="cont_crono">${t.remainMinutes}</span> <span class="cont_crono">${t.remainSeconds}</span> <br>
                      </div>
                      <div class="contador_name">
                        <span class="cont_name">DÍAS</span> <span class="cont_name">HORAS</span> <span class="cont_name">MIN.</span> <span class="cont_name">SEG.</span>
                      </div>
                    `;
    }
}
// export default Preloader;
module.exports = Chronometer;
