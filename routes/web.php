<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'StaticController@homePage')->name('home-page');
Route::get('/registro', 'StaticController@registro')->name('registro');
Route::get('/fin', 'StaticController@posRegistro')->name('fin');

Route::resource('participantes', 'ParticipantController');


Route::prefix('4dm1n')->group(function() {
  Auth::routes();
  Route::get('home', 'HomeController@index')->name('home');
  Route::get('reportes', 'HomeController@export')->name('reports');

});
