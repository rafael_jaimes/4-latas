<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class CompareDate {

    public static function CompareHigh($fin) {
      $fecha_actual = strtotime(date("d-m-Y H:i:00",time()));
      $fecha_entrada = strtotime($fin);
// "01-03-2019 12:00:00"

      if($fecha_actual >= $fecha_entrada)
        return(true);
      else
        return(false);

    }

    public static function CompareLess($inicio) {
      $fecha_actual = strtotime(date("d-m-Y H:i:00",time()));
      $fecha_inicio = strtotime($inicio);
// "01-03-2019 12:00:00"

      if($fecha_actual < $fecha_inicio)
        return(true);
      else
        return(false);

    }
}
