<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ParticipantStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return ([
              'name' => 'required|max:30|regex:/^[\pL\s\-]+$/u|min:2',
              'apellidos' => 'required|max:40|regex:/^[\pL\s\-]+$/u|min:2',
              'email' => 'required|unique:participants|email',
              'dni' => 'required|unique:participants|max:9|alpha_num',
              'phone' => 'required|numeric|max:999999999',
              'imgFront' => 'required|file',
              'imgBack' => 'file',
            ]);
    }

    public function messages()
    {
        return [
          'imgFront.image' => __("The imgFront must be an image."),
          'imgBack.image' => __("The imgBack must be an image."),
          'imgFront.required' => __("The imgFront field is required."),
          'imgBack.required' => __("The imgBack field is required.")
        ];
    }
}
