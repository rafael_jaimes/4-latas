<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Participant;

class StaticController extends Controller
{
    //
    public function homePage()
    {
        if(\CompareDate::CompareHigh("01-03-2019 12:00:00"))
          return redirect()->route('registro');

        $pre = true;
        return view('preRegistry.index', compact("pre"));
    }

    public function registro()
    {

      if(\CompareDate::CompareLess("01-03-2019 12:00:00"))
          return redirect()->route('home-page');

      if(\CompareDate::CompareHigh("04-03-2019 23:59:00"))
        return redirect()->route('fin');

      $pre = false;
      return view('registry.index', compact("pre"));
    }

    public function posRegistro()
    {
      if(\CompareDate::CompareLess("01-03-2019 23:59:00"))
          return redirect()->route('registro');

      $pre = false;

      return view('posRegistry.index', compact("pre"));

    }

}
