<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Participant;

USE App\Exports\ParticipatsExport;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $participants = Participant::orderBy('id', 'ASC')->paginate(8);
        return view('home',compact("participants"));
    }

    public function export(ParticipatsExport $participant)
    {
        return  $participant->download('participantes.xlsx');
    }
}
