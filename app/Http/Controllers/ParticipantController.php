<?php

namespace App\Http\Controllers;

use App\Models\Participant;
use App\Http\Requests\ParticipantStoreRequest;
use Illuminate\Http\Request;

class ParticipantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\ParticipantStoreRequest  $request
     * @return $atttributes
     */

    public function store(ParticipantStoreRequest $request)
    {
        $participant = Participant::create( $request->all() );

        // sube las imagenes y las guarda en la base
        if ($request->hasFile('imgFront')){
          $name = $participant->dni."-Frontal.". $request->imgFront->extension();
          $participant->imgFront = $request->file('imgFront')->storeAs('public/'.$participant->id, $name);
        }

        if ($request->hasFile('imgBack')){
          $nameback = $participant->dni."-Trasera.". $request->imgBack->extension();
          $participant->imgBack = $request->file('imgBack')->storeAs('public/'.$participant->id, $nameback);
        }

        $participant->save();

        return redirect()->route('registro')->with('message','Sus datos han sido guardados.');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Participant  $participant
     * @return \Illuminate\Http\Response
     */
    public function show(Participant $participant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Participant  $participant
     * @return \Illuminate\Http\Response
     */
    public function edit(Participant $participant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Participant  $participant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Participant $participant)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Participant  $participant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Participant $participant)
    {
        //
    }
}
