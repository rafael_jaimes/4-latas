<?php

namespace App\Exports;

use App\Models\Participant;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;


class ParticipatsExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    use Exportable;
    public function registerEvents(): array
   {
       return [
           AfterSheet::class    => function(AfterSheet $event) {
               $cellRange = 'A1:W1'; // All headers
               $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);

           },
       ];
   }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
      $Participants = Participant::all();
      foreach ($Participants as $key => $value) {
        // code...
        $value->imgFront = str_replace("public", "https://sorteo.4lataslapelicula.com/storage", $value->imgFront);
        $value->imgBack = str_replace("public", "https://sorteo.4lataslapelicula.com/storage", $value->imgFront);
      }
      // Product::select('id','name','description','serial','quantity')->get();
      // return Participant::all();
        return $Participants;
;
    }

    public function headings(): array
    {
        return [
            '#',
            'Nombre',
            'Apellidos',
            'Email',
            'DNI',
            'Teléfono',
            'Imagen Frontal',
            'Imagen Trasera',
            'created_at',
            'updated_at',
        ];
    }
}
